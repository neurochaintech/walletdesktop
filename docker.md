# Docker

## Install
```
sudo ./docker-install.sh
```

## Start
```
sudo service docker start
```

## List
```
docker ps
docker container list --all
```

## Start / Restart container
```
docker start containerId/containerName
docker restart containerId/containerName
```

## Stop container
```
docker stop containerId/containerName

docker kill $(docker ps -q)
```

## Stats hardware
```
docker system df -v
```

## Show logs
```
docker logs --follow containerId
```

## Login docker bash
```
docker exec -it containerId bash
```

## Exploring Docker container's file system
```
docker exec -t -i core /bin/bash
```

## Sending docker mongodb command
docker exec -i mongo mongo testnet --quiet --eval 'db.blocks.aggregate([{"$group" : {_id:"$block.header.author.keyPub.data", count:{$sum:1}}}]).toArray()'

## Remove containers
```
docker rm containerId

docker rm `docker ps -qa`
docker rm $(docker ps -a -q)
```

## List images available on PC
```
docker image ls
```

## Explore image files
```
docker exec -it containerID bash
docker run -it "neurochain/core/release" sh
```

## Remove unused images on PC
```
docker image prune
```