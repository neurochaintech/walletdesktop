## Install cmake 3.10

```
% Download and compile: 
cd ~/Downloads
wget http://www.cmake.org/files/v3.10/cmake-3.10.0.tar.gz
tar -xvzf cmake-3.10.0.tar.gz
cd cmake-3.10.0/
./configure
make 

% Make's install command installs cmake by default in /usr/local/bin/cmake, shared files are installed into /usr/local/share/cmake-3.10.
% To install (copy) the binary and libraries to the new destination, run: 
sudo make install

% Check
cmake --version

% Clean up
cd ~/Downloads
rm -rf cmake-3.10.0.tar.gz 
rm -rf cmake-3.10.0
```
