#include "./Tools/Crypto/ECDSATools.hpp"
#include "./Tools/SystemTools.hpp"

struct ExecSettings {
    std::string privateKeyPath  = "";
    std::string publicKeyPath   = "";
    std::string privateKeyBinary  = "";
    std::string publicKeyBinary   = "";
    bool decipher = false;
    bool generateKeys = false;

    void setup(const std::vector<std::string>& execArguments) {
        for (unsigned iArg = 0; iArg < execArguments.size(); ++iArg) {
            const std::string& arg = execArguments[iArg];

            if( arg.compare("-privateKeyPath") == 0 ) {
                this->privateKeyPath = execArguments[iArg + 1];
            
            } else if( arg.compare("-publicKeyPath") == 0 ) {
                this->publicKeyPath = execArguments[iArg + 1];
            
            } else if( arg.compare("-privateKeyBinary") == 0 ) {
                this->privateKeyBinary = execArguments[iArg + 1];
            
            } else if( arg.compare("-publicKeyBinary") == 0 ) {
                this->publicKeyBinary = execArguments[iArg + 1];
            
            }

            this->decipher |= arg.compare("-decipher") == 0;
            this->generateKeys |= arg.compare("-generateKeys") == 0;
        }
    }

    void showHelp() {
        std::cout << "-privateKeyPath: set private key file path" << std::endl;
        std::cout << "-publicKeyPath: set public key file path" << std::endl;
        std::cout << "-privateKeyBinary: set private key binary value" << std::endl;
        std::cout << "-publicKeyBinary: set public key binary value" << std::endl;
    }
};

int main(int argc, const char * argv[]) {
    std::string execPath = argv[0];
    std::vector<std::string> execArguments;
    if (argc > 1) {
        execArguments.assign(argv + 1, argv + argc);
    }

    ExecSettings settings;

    if( execArguments.size() == 0 || execArguments.front().compare("-h") == 0 || execArguments.front().compare("-help") == 0 ) {
        settings.showHelp();
    } else {
        settings.setup(execArguments);

        if( settings.privateKeyPath.length() > 0 && settings.publicKeyPath.length() > 0 ) {    

            if( settings.decipher ) {

                ECDSAWallet loadedWallet;

                try {
                    loadedWallet.loadFromFiles( settings.privateKeyPath, settings.publicKeyPath );
                } catch (const std::exception& e) { }

                std::cout << loadedWallet.toJson() << std::endl;

            } else if( settings.generateKeys ) {
                
                SystemTools::removeFile(settings.privateKeyPath);
                SystemTools::removeFile(settings.publicKeyPath);

                ECDSAWallet newWallet;
                const bool success = newWallet.generate(settings.privateKeyPath, settings.publicKeyPath);
                std::cout <<  "{ \"success\": " << success << " }" << std::endl;
            }
            
        }
        
    }

    return 0;
}