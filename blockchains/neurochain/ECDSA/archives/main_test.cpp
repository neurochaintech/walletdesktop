#include <iostream>
#include <string>
#include <sstream>

#include "base64.h"
#include "cryptlib.h"
#include "eccrypto.h"
#include "files.h"
#include "filters.h"
#include "hex.h"
#include "oids.h"
#include "osrng.h"

void removeLineBreaks( std::string& inputString ) {
    inputString.erase(std::remove(inputString.begin(), inputString.end(), '\n'), inputString.end());
}

struct Wallet {
    std::string publicKey = "";
    std::string privateKey = "";
    std::string privateKeyExponent = "";

    void loadPrivateKey(const std::string privateKeyPath) {
        std::shared_ptr<CryptoPP::AutoSeededRandomPool> prng = std::make_shared<CryptoPP::AutoSeededRandomPool>();
        CryptoPP::DL_GroupParameters_EC<CryptoPP::ECP> params = CryptoPP::DL_GroupParameters_EC<CryptoPP::ECP>(CryptoPP::ASN1::secp256k1());

        CryptoPP::FileSource fsPrivateKey(privateKeyPath.c_str(), true, new CryptoPP::Base64Encoder( new CryptoPP::StringSink(this->privateKey) ) );
        removeLineBreaks(this->privateKey);
        
        CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::PrivateKey cyptoPrivateKey;
        cyptoPrivateKey.Initialize(*prng, params);
        CryptoPP::FileSource fsPrivateKeyExponent(privateKeyPath.c_str(), true);
        cyptoPrivateKey.Load(fsPrivateKeyExponent);
        std::stringstream ssPrivateKey;
        ssPrivateKey << std::hex << cyptoPrivateKey.GetPrivateExponent();
        this->privateKeyExponent = ssPrivateKey.str();
    }

    /*
        Get the same result after private key import:
        CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::PublicKey cryptoPublicKey;
        this.privateKey.MakePublicKey(cryptoPublicKey);
        const CryptoPP::ECP::Point point = cryptoPublicKey.GetPublicElement();
    */
    void loadPublicKey(const std::string publicKeyPath) {
        CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::PublicKey cryptoPublicKey;
        CryptoPP::FileSource fsPublicKey(publicKeyPath.c_str(), true);
        cryptoPublicKey.Load(fsPublicKey);

        // Compressed Point (rf: https://www.cryptopp.com/wiki/Elliptic_Curve_Digital_Signature_Algorithm)
        const CryptoPP::ECP::Point point = cryptoPublicKey.GetPublicElement();
        
        const auto xSizeOutput = 32;
        CryptoPP::byte pointXHex[xSizeOutput + 1];
        point.x.Encode(&pointXHex[1], xSizeOutput);
        if (point.y.IsEven()) {
            pointXHex[0] = 0x02;
        } else {
            pointXHex[0] = 0x03;
        }
        std::string pointXBinary = std::string(reinterpret_cast<char *>(pointXHex), xSizeOutput + 1);

        CryptoPP::StringSource ssCompressed64(pointXBinary, true, new CryptoPP::Base64Encoder(new CryptoPP::StringSink(this->publicKey)));
        removeLineBreaks(this->publicKey);
    }

    void load( const std::string privateKeyPath, const std::string publicKeyPath ) {
        loadPrivateKey( privateKeyPath );
        loadPublicKey( publicKeyPath );
    }

    bool isEqual( const Wallet other ) {
        bool equal = true;

        equal &= this->publicKey.compare(other.publicKey) == 0;
        equal &= this->privateKey.compare(other.privateKey) == 0;
        equal &= this->privateKeyExponent.compare(other.privateKeyExponent) == 0;

        return equal;
    }

    void log() {
        std::cout << "Public Key: " << this->publicKey << " (length=" << this->publicKey.length() << ")" << std::endl;
        std::cout << "Private Key: " << this->privateKey << " (length=" << this->privateKey.length() << ")" << std::endl;
        std::cout << "Private Key Exponent: " << this->privateKeyExponent << " (length=" << this->privateKeyExponent.length() << ")" << std::endl;
    }
};

int main() {
    Wallet expected;
    expected.publicKey = "AiDnLkv2+YoCeoHacvucG+yYv0MAQlShs20w8VLsUgdq";
    expected.privateKey = "MD4CAQAwEAYHKoZIzj0CAQYFK4EEAAoEJzAlAgEBBCDnN27bsosMZb5SYCePsTfsCCEG8RbEmI1xZTmKflXuhQ==";
    expected.privateKeyExponent = "e7376edbb28b0c65be5260278fb137ec082106f116c4988d7165398a7e55ee85h";

    std::cout << "=======================" << std::endl;
    
    std::cout << "Expected values:" << std::endl;
    expected.log();

    std::cout << "=======================" << std::endl;

    const std::string privateKeyPath = "./wallet/key.priv";
    const std::string publicKeyPath = "./wallet/key.pub";

    Wallet hostWallet;
    hostWallet.load( privateKeyPath, publicKeyPath );

    std::cout << "Loaded values:" << std::endl;
    hostWallet.log();

    std::cout << "=======================" << std::endl;

    std::cout << "Loaded VS Expected" << std::endl;
    std::cout << "publicKey: ###" << hostWallet.publicKey << "### ?== ###" << expected.publicKey << "### => " << (hostWallet.publicKey.compare(expected.publicKey) == 0) << std::endl;
    std::cout << "privateKey: ###" << hostWallet.privateKey << "### ?== ###" << expected.privateKey << "### => " << (hostWallet.privateKey.compare(expected.privateKey) == 0) << std::endl;
    std::cout << "privateKeyExponent: ###" << hostWallet.privateKeyExponent << "### ?== ###" << expected.privateKeyExponent << "### => " << (hostWallet.privateKeyExponent.compare(expected.privateKeyExponent) == 0) << std::endl;

    std::cout << "Are values equal ? " << hostWallet.isEqual(expected) << std::endl;
}