<h1>Show keys values from</h1>

<h2>Run project</h2>

```
cd <path>/ECDSA
./run.sh
```

<h2>Cmake manually</h2>

Git commands are required

If directory "cryptopp" does not exist in ThirdParties directory :
1. clone https://github.com/weidai11/cryptopp.git
2. clone https://github.com/noloader/cryptopp-cmake.git
3. Copy all *.txt and *.cmake files from cryptopp-cmake to cryptopp
4. Remove cryptopp-cmake directory

Run cmake, build project :
```
cmake CMakeLists.txt
make
```

## Run C++ project
```
./projectName
```

## Where is the generated C++ project ?

In the CMakeLists.txt, check the line "set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ...)". In this project, the output is in the "./bin" directory.

<h2>Check bot's keys</h2>

<p>
Host destination: Testnet5-host5, ec2-35-181-125-1.eu-west-3.compute.amazonaws.com, 35.181.125.1
</p>

```
# Log on the host
ssh -i /Users/kim/Documents/Neurochain/ansible_awsInfraDev/Testnet5.pem ubuntu@ec2-35-181-125-1.eu-west-3.compute.amazonaws.com

# Log on core docker container
docker exec -t -i core /bin/bash

# If you want to know where the root is
pwd
/home/neuro

# Show keys
./showkey -k conf/key.priv -p conf/key.pub
public:
{"rawData":"AiDnLkv2+YoCeoHacvucG+yYv0MAQlShs20w8VLsUgdq"}
private:
{"data":"MD4CAQAwEAYHKoZIzj0CAQYFK4EEAAoEJzAlAgEBBCDnN27bsosMZb5SYCePsTfsCCEG8RbEmI1xZTmKflXuhQ=="}
private exponent:
e7376edbb28b0c65be5260278fb137ec082106f116c4988d7165398a7e55ee85h
address : 
{"data":"NHWWvB93d8c8xwjGv8rHZkgsJHA1tKGTyY"}
```

<h2>Export keys' files</h2>

```
# Log on the host
ssh -i /Users/kim/Documents/Neurochain/ansible_awsInfraDev/Testnet5.pem ubuntu@ec2-35-181-125-1.eu-west-3.compute.amazonaws.com

# Export from docker to host
docker cp core:/home/neuro/conf/key.priv ./neurochain_tmp
docker cp core:/home/neuro/conf/key.pub ./neurochain_tmp

# Export from host to local
exit
mkidr wallet
scp -i /Users/kim/Documents/Neurochain/ansible_awsInfraDev/Testnet5.pem -r ubuntu@ec2-35-181-125-1.eu-west-3.compute.amazonaws.com:~/neurochain_tmp/key.priv ./wallet
scp -i /Users/kim/Documents/Neurochain/ansible_awsInfraDev/Testnet5.pem -r ubuntu@ec2-35-181-125-1.eu-west-3.compute.amazonaws.com:~/neurochain_tmp/key.pub ./wallet
```

