#include "TextTools.hpp"

void TextTools::removeLineBreaks( std::string& inputString ) {
    inputString.erase( std::remove(inputString.begin(), inputString.end(), '\n'), inputString.end() );
}