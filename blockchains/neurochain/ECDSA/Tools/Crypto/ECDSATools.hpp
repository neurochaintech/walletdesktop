#ifndef ECDSATools_hpp
#define ECDSATools_hpp

#include <iostream>
#include <string>
#include <sstream>
#include <vector>

#include "base64.h"
#include "cryptlib.h"
#include "eccrypto.h"
#include "files.h"
#include "filters.h"
#include "hex.h"
#include "oids.h"
#include "osrng.h"

#include "../TextTools.hpp"

struct ECDSAWallet {
    std::string publicKey = "";
    std::string privateKey = "";
    std::string privateKeyExponent = "";

    bool generate(const std::string privateKeyPath, const std::string publicKeyPath) {
        bool success = false;

        try {
            std::shared_ptr<CryptoPP::AutoSeededRandomPool> prng = std::make_shared<CryptoPP::AutoSeededRandomPool>();
            CryptoPP::DL_GroupParameters_EC<CryptoPP::ECP> params = CryptoPP::DL_GroupParameters_EC<CryptoPP::ECP>(CryptoPP::ASN1::secp256k1());
            CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::PrivateKey cyptoPrivateKey;
            cyptoPrivateKey.Initialize(*prng, params);

            CryptoPP::FileSink fsPrivate( privateKeyPath.c_str(), true );
            cyptoPrivateKey.Save(fsPrivate);
            
            CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::PublicKey cryptoPublicKey;
            cyptoPrivateKey.MakePublicKey(cryptoPublicKey);
            CryptoPP::FileSink fsPublic( publicKeyPath.c_str(), true );
            cryptoPublicKey.Save(fsPublic);

            success = true;
        } catch (const std::exception& e) { }

        return success;
    }

    void loadPrivateKeyFromFile(const std::string privateKeyPath) {
        CryptoPP::FileSource fsPrivateKey(privateKeyPath.c_str(), true, new CryptoPP::Base64Encoder( new CryptoPP::StringSink(this->privateKey) ) );
        TextTools::removeLineBreaks(this->privateKey);
        
        std::shared_ptr<CryptoPP::AutoSeededRandomPool> prng = std::make_shared<CryptoPP::AutoSeededRandomPool>();
        CryptoPP::DL_GroupParameters_EC<CryptoPP::ECP> params = CryptoPP::DL_GroupParameters_EC<CryptoPP::ECP>(CryptoPP::ASN1::secp256k1());
        CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::PrivateKey cyptoPrivateKey;
        cyptoPrivateKey.Initialize(*prng, params);
        CryptoPP::FileSource fsPrivateKeyExponent(privateKeyPath.c_str(), true);
        cyptoPrivateKey.Load(fsPrivateKeyExponent);
        std::stringstream ssPrivateKey;
        ssPrivateKey << std::hex << cyptoPrivateKey.GetPrivateExponent();
        this->privateKeyExponent = ssPrivateKey.str();
    }

    /*
        Get the same result after private key import:
        CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::PrivateKey cyptoPrivateKey;
        ...
        CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::PublicKey cryptoPublicKey;
        this.privateKey.MakePublicKey(cryptoPublicKey);
        const CryptoPP::ECP::Point point = cryptoPublicKey.GetPublicElement();
    */
    void loadPublicKeyFromFile(const std::string publicKeyPath) {
        CryptoPP::ECDSA<CryptoPP::ECP, CryptoPP::SHA256>::PublicKey cryptoPublicKey;
        CryptoPP::FileSource fsPublicKey(publicKeyPath.c_str(), true);
        cryptoPublicKey.Load(fsPublicKey);

        // Compressed Point (rf: https://www.cryptopp.com/wiki/Elliptic_Curve_Digital_Signature_Algorithm)
        const CryptoPP::ECP::Point point = cryptoPublicKey.GetPublicElement();
        
        const auto xSizeOutput = 32;
        CryptoPP::byte pointXHex[xSizeOutput + 1];
        point.x.Encode(&pointXHex[1], xSizeOutput);
        if (point.y.IsEven()) {
            pointXHex[0] = 0x02;
        } else {
            pointXHex[0] = 0x03;
        }
        std::string pointXBinary = std::string(reinterpret_cast<char *>(pointXHex), xSizeOutput + 1);

        CryptoPP::StringSource ssCompressed64(pointXBinary, true, new CryptoPP::Base64Encoder(new CryptoPP::StringSink(this->publicKey)));
        TextTools::removeLineBreaks(this->publicKey);
    }

    void loadFromFiles( const std::string privateKeyPath, const std::string publicKeyPath ) {
        loadPrivateKeyFromFile( privateKeyPath );
        loadPublicKeyFromFile( publicKeyPath );
    }

    bool isEqual( const ECDSAWallet& other ) {
        bool equal = true;

        equal &= this->publicKey.compare(other.publicKey) == 0;
        equal &= this->privateKey.compare(other.privateKey) == 0;
        equal &= this->privateKeyExponent.compare(other.privateKeyExponent) == 0;

        return equal;
    }

    std::string toJson() {
        std::string jsonString = "{";
        jsonString += " \"publicKey\": \"" + this->publicKey + "\" ,";
        jsonString += " \"privateKey\": \"" + this->privateKey + "\" ,";
        jsonString += " \"privateKeyExponent\": \"" + this->privateKeyExponent + "\" ";
        jsonString += "}";

        return jsonString;
    }
};

#endif