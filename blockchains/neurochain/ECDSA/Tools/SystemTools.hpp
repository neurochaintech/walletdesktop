#ifndef SystemTools_hpp
#define SystemTools_hpp

#include <stdio.h>
#include <string>
#include <iostream>
#include <vector>

#include "FileTypesEnum.hpp"

class SystemTools {

public:
    static bool doesExist(const std::string& path, const FileType& fileType);
    
    static void createFile(const std::string& path);
    static void removeFile(const std::string& path);
    static void appendToFile(const std::string& path, const std::string& text);
};

#endif