#ifndef EncoderTools_hpp
#define EncoderTools_hpp

#include <iostream>
#include <string>
#include <vector>
typedef unsigned char BYTE;

class EncoderTools {
public:
    static std::string base64_encode(BYTE const* buf, unsigned int bufLen);
    static std::vector<BYTE> base64_decode(std::string const&);
};

#endif