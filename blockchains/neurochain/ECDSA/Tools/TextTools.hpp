#ifndef TextTools_hpp
#define TextTools_hpp

#include <stdio.h>
#include <string>
#include <vector>
#include <algorithm>

class TextTools {
public:
    static void removeLineBreaks( std::string& inputString );
};

#endif