#include "SystemTools.hpp"

#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>

bool SystemTools::doesExist(const std::string& path, const FileType& fileType) {
    bool exist = false;
    
    if( !path.empty() )
    {
        if( access(path.c_str(), 0) == 0 )
        {
            struct stat status;
            stat( path.c_str(), &status );
            
            if(fileType == FileType::file) {
                if( !(status.st_mode & S_IFDIR) ) {
                    exist = true;
                }
            } else if(fileType == FileType::directory)  {
                if( status.st_mode & S_IFDIR ) {
                    exist = true;
                }
            }
            
        }
    }

    return exist;
}


void SystemTools::createFile(const std::string& path) {
    std::ofstream file(path);
    file.close();
}

void SystemTools::removeFile(const std::string& path) {
    if( doesExist(path, FileType::file) ) {
        std::cout << "Delete file : " << path << std::endl;
    
        std::string command = "rm " + path;
        system(command.c_str());
    }
}

void SystemTools::appendToFile(const std::string& path, const std::string& text) {
    std::ofstream file;
    file.open(path, std::ios_base::app);
    file << text << std::endl;
    file.close();
}