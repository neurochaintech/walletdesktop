#!/bin/bash

clear

cmake CMakeLists.txt
make

./bin/ECDSA -decipher -privateKeyPath ./wallet/key.priv -publicKeyPath ./wallet/key.pub

./bin/ECDSA -generateKeys -privateKeyPath ./wallet/newKey.priv -publicKeyPath ./wallet/newKey.pub
./bin/ECDSA -decipher -privateKeyPath ./wallet/newKey.priv -publicKeyPath ./wallet/newKey.pub

