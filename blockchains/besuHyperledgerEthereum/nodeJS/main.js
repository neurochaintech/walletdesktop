"use strict";

import Web3 from 'web3';

const BLOCKCHIN_PORT = 8545;
const BLOCKCHIN_ADDRESS = `http://localhost:${BLOCKCHIN_PORT}`;

async function main() {
    try {
        const web3 = new Web3(BLOCKCHIN_ADDRESS);
        console.log(`Connected on: ${BLOCKCHIN_ADDRESS}`);

        // const blockCount = await web3.eth.getBlockNumber();
        // console.log(`Blocks count: ${blockCount}`);
        await web3.eth.getBlockNumber().then(console.log);

        // Nodes
        await web3.eth.getBalance("0x5d4e0c9ed11deda9d3f795f81a1ca2cd892c2a70").then(console.log);
        await web3.eth.getBalance("0xc67e6a690d255eabedd803b33e3db07368dfe3fb").then(console.log);
        await web3.eth.getBalance("0xc798402a84c18a477cfa62970e9b3d1256a4908f").then(console.log);
        await web3.eth.getBalance("0xd36c8a90f8fc86aa6214869bac2125f15d8d4e15").then(console.log);

        // Wallets
        await web3.eth.getBalance("0xfe3b557e8fb62b89f4916b721be55ceb828dbd73").then(console.log);
        await web3.eth.getBalance("0x627306090abaB3A6e1400e9345bC60c78a8BEf57").then(console.log);
        await web3.eth.getBalance("0xf17f52151EbEF6C7334FAD080c5704D77216b732").then(console.log);

        // Send transactions
        // const walletAuthor = {
        //     publicKey: "627306090abaB3A6e1400e9345bC60c78a8BEf57",
        //     privateKey: "c87509a1c067bbde78beb793e6fa76530b6382a4c0241e5e4a9ec0a0f44dc0d3"
        // }

        // const nonce = await web3.eth.getTransactionCount(`0x${walletAuthor.publicKey}`, 'latest');
        // const transaction = {
        //     'to': '0xfe3b557e8fb62b89f4916b721be55ceb828dbd73',
        //     'value': 1000000000000000000, // 1 ETH
        //     'gas': 30000,
        //     'nonce': nonce
        // };
        // const signedTx = await web3.eth.accounts.signTransaction(transaction, walletAuthor.privateKey);
        // await web3.eth.sendSignedTransaction(signedTx.rawTransaction, function(error, hash) {
        //     if (error) {
        //         console.log(error);
        //     } else {
        //         console.log(hash);
        //     }
        // });
        
        // // Wallets
        // await web3.eth.getBalance("0xfe3b557e8fb62b89f4916b721be55ceb828dbd73").then(console.log);
        // await web3.eth.getBalance("0x627306090abaB3A6e1400e9345bC60c78a8BEf57").then(console.log);
        // await web3.eth.getBalance("0xf17f52151EbEF6C7334FAD080c5704D77216b732").then(console.log);

        process.exit(0);
    } catch(e) {
        console.error(e);
        process.exit(1);
    }
};
main();