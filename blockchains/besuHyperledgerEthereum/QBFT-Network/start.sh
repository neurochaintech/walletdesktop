#!/bin/bash

set -e

if [[ "$*" == *"init"* ]]
then
    echo "INIT"

    echo "Clean up"
    for node in */ ; do
        [ -L "${d%/}" ] && continue
        echo "Clean up $node"

        dataPath="./$node/data"
        rm -rf "$dataPath"
        mkdir "$dataPath"
    done

    echo "Generate configs"
    rm -rf "./networkFiles"
    ../besu/bin/besu operator generate-blockchain-config --config-file=qbftConfigFile_freeGas.json --to=networkFiles --private-key-file-name=key
    cp ./networkFiles/genesis.json ./genesis.json
fi

echo "START NODES"
for node in */ ; do
    [ -L "${d%/}" ] && continue
    [[ "$node" == *"networkFiles"* ]] && continue

    echo "Start $node"
    command="../besu/bin/besu --config-file=./$node/config.toml"
    gnome-terminal -- bash -c "$command; exec bash;"
done
