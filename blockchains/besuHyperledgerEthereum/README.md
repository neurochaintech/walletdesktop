# Besu Hyperledger

## Install 

JDK
```
sudo apt-get update && sudo apt-get install openjdk-11-jdk

# Check
java --version

openjdk 11.0.16 2022-07-19
OpenJDK Runtime Environment (build 11.0.16+8-post-Ubuntu-0ubuntu118.04)
OpenJDK 64-Bit Server VM (build 11.0.16+8-post-Ubuntu-0ubuntu118.04, mixed mode, sharing)
```

Check https://github.com/hyperledger/besu/releases and choose the version
```
wget -c https://hyperledger.jfrog.io/hyperledger/besu-binaries/besu/22.7.6/besu-22.7.6.zip -O besu.zip
unzip besu.zip
mv -T besu-22.7.6 besu

# Check: show help
./besu/bin/besu --help
```

## Run private network

### QBFT

This private network uses the QBFT (proof of authority) consensus protocol. QBFT is the recommended enterprise-grade consensus protocol for private networks.

In QBFT networks, approved accounts, known as validators, validate transactions and blocks. Validators take turns to create the next block. Before inserting the block onto the chain, a super-majority (greater than or equal to 2/3) of validators must first sign the block. QBFT requires 4 validators to be Byzantine fault tolerant.

This private network is NOT protected or secure: it's strongly recommed to run the private network behind a properly configured firewall.

```
cd QBFT-Network
sudo ./start.sh
sudo ./start.sh -init
```

Check
```
curl -X POST --data '{"jsonrpc":"2.0","method":"qbft_getValidatorsByBlockNumber","params":["latest"], "id":1}' localhost:8545

{
  "jsonrpc" : "2.0",
  "id" : 1,
  "result" : [ "0x4770bf41be10e89843ff94b88b6cddf0174bae5f", "0x8bcd4f6cb9ea7e1ba177f75c2af943ea7d1b4778", "0xd23834551657def6a770610ad0181666e39ac2cf", "0xe09ae547dea131542bce8d77cbc0026b54629c18" ]
}
```

## Available accounts

Check the directories "networkFiles"

## Basics

Send transaction

Get Transaction

Count blocks

Get block

Transactions author for public key

Transactions receiver for public key

## Create and add a new node in the network

## If no more space on disk