```
tree

.
├── genesis.json
├── ibftConfigFile.json
├── networkFiles
│   ├── genesis.json
│   └── keys
│       ├── 0x5d4e0c9ed11deda9d3f795f81a1ca2cd892c2a70
│       │   ├── key
│       │   └── key.pub
│       ├── 0xc67e6a690d255eabedd803b33e3db07368dfe3fb
│       │   ├── key
│       │   └── key.pub
│       ├── 0xc798402a84c18a477cfa62970e9b3d1256a4908f
│       │   ├── key
│       │   └── key.pub
│       └── 0xd36c8a90f8fc86aa6214869bac2125f15d8d4e15
│           ├── key
│           └── key.pub
├── Node-1
│   └── data
│       ├── key
│       ├── key.pub
├── Node-2
│   └── data
│       ├── key
│       └── key.pub
├── Node-3
│   └── data
│       ├── key
│       └── key.pub
├── Node-4
│   └── data
│       ├── key
│       └── key.pub
├── README.md
└── start.sh

```