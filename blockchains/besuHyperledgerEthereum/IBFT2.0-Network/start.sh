#!/bin/bash

set -e

# Init

# ../besu/bin/besu operator generate-blockchain-config --config-file=ibftConfigFile.json --to=networkFiles --private-key-file-name=key
# cp ./networkFiles/genesis.json ./genesis.json

# cp ./networkFiles/keys/0x5d4e0c9ed11deda9d3f795f81a1ca2cd892c2a70/key ./Node-1/data/key
# cp ./networkFiles/keys/0x5d4e0c9ed11deda9d3f795f81a1ca2cd892c2a70/key.pub ./Node-1/data/key.pub

# cp ./networkFiles/keys/0xc67e6a690d255eabedd803b33e3db07368dfe3fb/key ./Node-2/data/key
# cp ./networkFiles/keys/0xc67e6a690d255eabedd803b33e3db07368dfe3fb/key.pub ./Node-2/data/key.pub

# cp ./networkFiles/keys/0xc798402a84c18a477cfa62970e9b3d1256a4908f/key ./Node-3/data/key
# cp ./networkFiles/keys/0xc798402a84c18a477cfa62970e9b3d1256a4908f/key.pub ./Node-3/data/key.pub

# cp ./networkFiles/keys/0xd36c8a90f8fc86aa6214869bac2125f15d8d4e15/key ./Node-4/data/key
# cp ./networkFiles/keys/0xd36c8a90f8fc86aa6214869bac2125f15d8d4e15/key.pub ./Node-4/data/key.pub

# Launch nodes

commandNode1="../../besu/bin/besu --data-path=data --genesis-file=../genesis.json --rpc-http-enabled --rpc-http-api=ETH,NET,IBFT --host-allowlist="*" --rpc-http-cors-origins=\"all\""
gnome-terminal -- bash -c "cd ./Node-1; $commandNode1; exec bash;"
# 2022-11-09 13:51:09.053+01:00 | main | INFO  | DefaultP2PNetwork | Enode URL enode://a0fe8091156ccfdeee8fac796cec78f50bc6878c02c1a82b3b0fd9d94f525f9c87f965939e5cc5ff5bf9a5c64c0b4933ef3cce312161ae0cda8f58c9b2646a5b@127.0.0.1:30303

commandNode2="../../besu/bin/besu --data-path=data --genesis-file=../genesis.json --bootnodes=enode://a0fe8091156ccfdeee8fac796cec78f50bc6878c02c1a82b3b0fd9d94f525f9c87f965939e5cc5ff5bf9a5c64c0b4933ef3cce312161ae0cda8f58c9b2646a5b@127.0.0.1:30303 --p2p-port=30304 --rpc-http-enabled --rpc-http-api=ETH,NET,IBFT --host-allowlist=\"*\" --rpc-http-cors-origins=\"all\" --rpc-http-port=8546"
gnome-terminal -- bash -c "cd ./Node-2; $commandNode2; exec bash;"

commandNode3="../../besu/bin/besu --data-path=data --genesis-file=../genesis.json --bootnodes=enode://a0fe8091156ccfdeee8fac796cec78f50bc6878c02c1a82b3b0fd9d94f525f9c87f965939e5cc5ff5bf9a5c64c0b4933ef3cce312161ae0cda8f58c9b2646a5b@127.0.0.1:30303 --p2p-port=30305 --rpc-http-enabled --rpc-http-api=ETH,NET,IBFT --host-allowlist=\"*\" --rpc-http-cors-origins=\"all\" --rpc-http-port=8547"
gnome-terminal -- bash -c "cd ./Node-3; $commandNode3; exec bash;"

commandNode4="../../besu/bin/besu --data-path=data --genesis-file=../genesis.json --bootnodes=enode://a0fe8091156ccfdeee8fac796cec78f50bc6878c02c1a82b3b0fd9d94f525f9c87f965939e5cc5ff5bf9a5c64c0b4933ef3cce312161ae0cda8f58c9b2646a5b@127.0.0.1:30303 --p2p-port=30306 --rpc-http-enabled --rpc-http-api=ETH,NET,IBFT --host-allowlist=\"*\" --rpc-http-cors-origins=\"all\" --rpc-http-port=8548"
gnome-terminal -- bash -c "cd ./Node-4; $commandNode4; exec bash;"