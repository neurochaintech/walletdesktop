# Wallet Desktop

Desktop application to create and manage wallets with Vault by HashiCorp (https://www.vaultproject.io/)

## Prerequisites

```
cd app
npm install
```

## Launch

```
cd app
npm start
```